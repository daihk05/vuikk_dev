<?php

// Error reporting:
error_reporting(E_ALL^E_NOTICE);


include('../db.php');

//Comments Data

class Comment
{

	private $data = array();
	
	public function __construct($row)
	{
		/*
		/	The constructor
		*/
		
		$this->data = $row;
	}
	
	public function markup($text)
	{
		/*
		/	This method outputs the XHTML markup of the comment
		*/
		
		return '
			<span>'.$text.'</span>
		';
	}
	
	public static function validate(&$arr)
	{
		/*
		/	This method is used to validate the data sent via AJAX.
		/
		/	It return true/false depending on whether the data is valid, and populates
		/	the $arr array passed as a paremter (notice the ampersand above) with
		/	either the valid input data, or the error messages.
		*/
		
		$errors = array();
		$data	= array();
		
		// Using the filter with a custom callback function:
		
		if(!($data['editcomment'] = filter_input(INPUT_POST,'editcomment',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['editcomment'] = 'Sai định dạng.';
		}
		
		if(!($data['editcid'] = filter_input(INPUT_POST,'editcid',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['editcid'] = 'Đã xảy ra lỗi.';
		}
		
		if(!empty($errors)){
			
			// If there are errors, copy the $errors array to $arr:
			
			$arr = $errors;
			return false;
		}
		
		// If the data is valid, sanitize all the data and copy it to $arr:
		
		foreach($data as $k=>$v){
			$arr[$k] = ($v);
		}
				
		return true;
		
	}

	private static function validate_text($str)
	{
		/*
		/	This method is used internally as a FILTER_CALLBACK
		*/
		
		if(mb_strlen($str,'utf8')<1)
			return false;
		
		// Encode all html special characters (<, >, ", & .. etc) and convert
		// the new line characters to <br> tags:
		
		$str = nl2br(htmlspecialchars($str));
		
		// Remove the new line characters that are left
		$str = str_replace(array(chr(10),chr(13)),'',$str);
		
		return $str;
	}

}


/*
/	This array is going to be populated with either
/	the data that was sent to the script, or the
/	error messages.
/*/

$arr = array();
$validates = Comment::validate($arr);

if($validates)
{
	/* Everything is OK, insert to database: */
	
	$CommentsTxt = $mysqli->escape_string($arr['editcomment']);
	
	$mysqli->query("UPDATE comments SET comment='".$CommentsTxt."', edited=1 WHERE id='".$arr['editcid']."'");
	
	$arr['date'] = date('r',time());
	
	$arr = array_map('stripslashes',$arr);
	
	$insertedComment = new Comment($arr);

	
	echo json_encode(array('status'=>1,'html'=>$insertedComment->markup($CommentsTxt)));

}
else
{
	/* Outputtng the error messages */
	echo '{"status":0,"errors":'.json_encode($arr).'}';
}

?>
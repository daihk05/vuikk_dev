<!-- "Post button" -->
<div class="post-box">
<div class="post-media" id="post-button">
  <h3 style="color: #656565;"><?php echo $Settings['slogan']; ?></h3>
  <br>
	<?php if(isset($_SESSION['useremail'])){ ?>
	  <a id="postimage" href="javascript:void(0)" data-post="<?php echo checkNumPosts(); ?>" data-name="image" onclick="postMedia(this)"><button type="button" class="post-button"><i class="glyphicon glyphicon-picture"></i> &nbsp; <span style="color: #4885ed">Đăng ảnh vui</span></button></a>
	  <a id="postvideo" href="javascript:void(0)" data-post="<?php echo checkNumPosts(); ?>" data-name="video" onclick="postMedia(this)"><button type="button" class="post-button"><i class="glyphicon glyphicon-film"></i> &nbsp; <span style="color: #4885ed">Đăng video vui</span></button></a>
	<?php }	else { ?>
		<a data-toggle="modal" data-target="#myModal" href="javascript:void(0)"><button type="button" class="post-button"><i class="glyphicon glyphicon-picture"></i> &nbsp; <span style="color: #4885ed">Đăng ảnh vui</span></button></a>
	  	<a data-toggle="modal" data-target="#myModal" href="javascript:void(0)"><button type="button" class="post-button"><i class="glyphicon glyphicon-film"></i> &nbsp; <span style="color: #4885ed">Đăng video vui</span></button></a>
	<?php } ?>
  </div>
</div>

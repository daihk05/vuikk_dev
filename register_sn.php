<?php include('header.php'); ?>

<div class="container">

<div class="col-md-8" id="left">

<script>
$(document).ready(function()
{
    $('#FromSnRegister').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info"> Đang tải ...</div>'); 
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});

</script>

<div class="post-box">
<header class="post-header"><div class="post-title"><h1>Thông tin tài khoản</h1></div><!--post-title--></header>

<?php if(!isset($_SESSION['useremail'])){?>

<form action="submit_user_sn.php" id="FromSnRegister" method="post" style="margin-left: 10px; margin-right: 10px;">

<div id="output"></div>

<div class="form-group">
    <label for="uNameSn">Tên hiển thị</label>
    <input type="text" class="form-control" name="uNameSn" id="uNameSn" value="<?php echo $_SESSION['sn_name']; ?>" placeholder="Nhập tên hiển thị" />
</div>
<div class="form-group">    
     <label for="uEmailSn">Email</label>
    <input type="text" class="form-control" name="uEmailSn" id="uEmailSn" value="<?php echo $_SESSION['sn_email']; ?>" placeholder="Nhập email" />
</div>
     
  <button class="btn btn-default btn-primary pull-right submitButton" style="margin-bottom: 15px;">Xác nhận</button>

</form>

<?php 
}

else{?>

<div class="alert alert-danger" role="alert">Đã được đăng ký!</div>


<?php }?>
</div><!--post-box-->

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");

?>


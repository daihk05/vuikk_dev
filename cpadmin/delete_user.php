<?php

include("../../db.php");

if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

	$Settings = mysqli_fetch_array($SettingsSql);

	$SettingsSql->close();
}else{
	?><script>errorpage();</script><?php
}

//Data lake
require_once '../datalake/vendor/autoload.php';
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
//Access API
$ACCOUNT_NAME      = $Settings['dataname'];
$ACCOUNT_KEY       = $Settings['data_key'];
$connectionString  = "DefaultEndpointsProtocol=https;AccountName=".$ACCOUNT_NAME.";AccountKey=".$ACCOUNT_KEY;
$blobClient        = BlobRestProxy::createBlobService($connectionString); // Create blob client.
$containerNameAvatars = "data/avatars"; //Upload container
$containerNameUploads = "data/uploads";

$del = $mysqli->escape_string($_POST['id']);

//Delete avatar
if($Avatar = $mysqli->query("SELECT * FROM users WHERE uid='$del'")){
	$GetAvatar = mysqli_fetch_array($Avatar);
	
	$UserAvatar = $GetAvatar['avatar'];
	
	if (!empty($UserAvatar)){
		try {
			// Delete old avatar
			$blobClient->deleteBlob($containerNameAvatars, $UserAvatar);
		}
		catch(ServiceException $e){
			$code = $e->getCode();
			$error_message = $e->getMessage();
		}
	}
	$Avatar->close();
	
}else{ 
	?><script>errorpage();</script><?php
}


//Delete image
if($ImageInfo = $mysqli->query("SELECT * FROM media WHERE uid='$del'")){

	while ($GetInfo = mysqli_fetch_array($ImageInfo)){
		
		$Image = $GetInfo['image'];
	
		if (!empty($Image)){
		try {
			//Delete blob
			$blobClient->deleteBlob($containerNameUploads, $Image);
		}
		catch(ServiceException $e){
			$code = $e->getCode();
			$error_message = $e->getMessage();
		}
		}
	
	}		
	$ImageInfo->close();
	
	//Delete posts in media
	$DeletePosts = $mysqli->query("DELETE FROM media WHERE uid='$del'");
		
}else{
	?><script>errorpage();</script><?php
}

//Delete comments in comments
$DeleteComments = $mysqli->query("DELETE FROM comments WHERE uid='$del'");

//Delete votes in voteip and votecmt
$DeleteVotesIp = $mysqli->query("DELETE FROM voteip WHERE uid='$del'");
$DeleteVotesCmt = $mysqli->query("DELETE FROM votecmt WHERE uid='$del'");

//Delete user in db
$DeleteUsers = $mysqli->query("DELETE FROM users WHERE uid='$del'");

echo '<div class="alert alert-success" role="alert">Đã xóa xong.</div>';

?>
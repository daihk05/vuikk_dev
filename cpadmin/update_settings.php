<?php

include('../../db.php');

//Get User Settings

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$Active = $Settings['active'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("Error: %s\n", $mysqli->error);
}

//Get User Info

if($_POST)
{	

	if(!isset($_POST['inputTitle']) || strlen($_POST['inputTitle'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your site title</div>');
	}
	
	if(!isset($_POST['inputSiteurl']) || strlen($_POST['inputSiteurl'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your site link</div>');
	}
		
	if(!isset($_POST['inputEmail']) || strlen($_POST['inputEmail'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Admin Email Address</div>');
	}

	if(!isset($_POST['inputFbapp']) || strlen($_POST['inputFbapp'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Facebook App ID</div>');
	}

	if(!isset($_POST['inputFbSecret']) || strlen($_POST['inputFbSecret'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Facebook App Secret</div>');
	}

	if(!isset($_POST['inputGgapp']) || strlen($_POST['inputGgapp'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Google App ID</div>');
	}

	if(!isset($_POST['inputGgSecret']) || strlen($_POST['inputGgSecret'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Google App Secret</div>');
	}

	if(!isset($_POST['inputDataName']) || strlen($_POST['inputDataName'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Cloud Data Name</div>');
	}

	if(!isset($_POST['inputDataKey']) || strlen($_POST['inputDataKey'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Cloud Data Key</div>');
	}

	if(!isset($_POST['inputDataLink']) || strlen($_POST['inputDataLink'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Data Link</div>');
	}

	if(!isset($_POST['inputSendEmail']) || strlen($_POST['inputSendEmail'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Send code Email</div>');
	}

	if(!isset($_POST['inputSendPass']) || strlen($_POST['inputSendPass'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Please enter your Send code Pass</div>');
	}

	if(!isset($_POST['inputFbpage']) || strlen($_POST['inputFbpage'])>1)
	{
	$CheckFacebookPage = $mysqli->escape_string($_POST['inputFbpage']);

	if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $CheckFacebookPage)) {
  		//do nothing
	}else {
  	
	die('<div class="alert alert-danger" role="alert">Please enter full Facebook url.</div>');
	
	}
	}
		
	//Title
	$SiteTitle			= $mysqli->escape_string($_POST['inputTitle']); // site title
	$SiteLink           = $mysqli->escape_string($_POST['inputSiteurl']);
	$MetaDescription    = $mysqli->escape_string($_POST['inputDescription']);
	$MetaKeywords 		= $mysqli->escape_string($_POST['inputKeywords']);
	$SiteEmail		    = $mysqli->escape_string($_POST['inputEmail']);
	//GUI setting
	$OpenPosts		    = $mysqli->escape_string($_POST['inputOpenPosts']);
	$IdlePop		    = $mysqli->escape_string($_POST['inputIdle']);
	//ID and Key
	$FBApp       		= $mysqli->escape_string($_POST['inputFbapp']);
	$FBSecret      		= $mysqli->escape_string($_POST['inputFbSecret']);
	$GGApp      		= $mysqli->escape_string($_POST['inputGgapp']);
	$GGSecret      		= $mysqli->escape_string($_POST['inputGgSecret']);
	$DataName      		= $mysqli->escape_string($_POST['inputDataName']);
	$DataKey      		= $mysqli->escape_string($_POST['inputDataKey']);
	$DataLink      		= $mysqli->escape_string($_POST['inputDataLink']);
	//Page and send active code
	$FBPage	       		= $mysqli->escape_string($_POST['inputFbpage']);
	$GACode			    = $mysqli->escape_string($_POST['inputGA']);
	$SendEmail			= $mysqli->escape_string($_POST['inputSendEmail']);
	$SendPass			= $mysqli->escape_string($_POST['inputSendPass']);
	$Approve		    = $mysqli->escape_string($_POST['inputApprove']);
	//Time limit
	$LimitHome		    = $mysqli->escape_string($_POST['inputLimitHome']);
	$LimitHot		    = $mysqli->escape_string($_POST['inputLimitHot']);
	$LimitDelete		= $mysqli->escape_string($_POST['inputLimitDelete']);
	$LimitPost		    = $mysqli->escape_string($_POST['inputLimitPost']);
	$LimitPoint		    = $mysqli->escape_string($_POST['inputLimitPoint']);
	$LimitReport	    = $mysqli->escape_string($_POST['inputLimitReport']);
		
	//Save to db
	$mysqli->query("UPDATE settings SET name='$SiteTitle',siteurl='$SiteLink',keywords='$MetaKeywords',descrp='$MetaDescription',email='$SiteEmail',sendemail='$SendEmail',sendpass='$SendPass',fbapp='$FBApp',fb_key='$FBSecret',ggapp='$GGApp',gg_key='$GGSecret',dataname='$DataName',data_key='$DataKey',datalink='$DataLink',active='$Approve',ga_code='$GACode',open_posts='$OpenPosts',idle_popup='$IdlePop',fbpage='$FBPage',limit_posts='$LimitPost',limit_home='$LimitHome',limit_hot='$LimitHot',limit_delete='$LimitDelete',limit_points='$LimitPoint',limit_reports='$LimitReport' WHERE id=1");

	die('<div class="alert alert-success" role="alert">Site settings updated successfully.</div>');

}else{
	die('<div class="alert alert-danger" role="alert">Đã xảy ra sự cố! Vui lòng thử lại!</div>');

}
?>
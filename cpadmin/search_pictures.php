<?php include("header.php");?>
<section class="col-md-2">

<?php include("left_menu.php");

$term = $mysqli->escape_string($_GET['term']);
$status = $mysqli->escape_string($_GET['status']);

$term=trim($term)
?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Quản trị</li>
  <li>Bài đăng</li>
  <li class="active">Tìm kiếm ảnh <?php if($status==1){ echo "đã duyệt";}elseif($status==0){ echo "chưa duyệt";}?></li>
</ol>

<div class="page-header">
  <h3>Ảnh <?php if($status==1){ echo "đã duyệt";}elseif($status==0){ echo "chưa duyệt";}?> <small>tìm kiếm</small></h3>
</div>

<script type="text/javascript">
$(document).ready(function(){
//Delete	
$('button.btnDelete').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#myModal').data('id', id).modal('show');
});

$('#btnDelteYes').click(function () {
    var id = $('#myModal').data('id');
	var dataString = 'id='+ id ;
    $('[data-id=' + id + ']').remove();
    $('#myModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "delete_post.php",
data: dataString,
cache: false,
success: function(html)
{
//$(".fav-count").html(html);
$("#output").html(html);
}
});
//ajax ends
});
//Desapprove
$('button.btnDisapprove').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#DisapproveModal').data('id', id).modal('show');
});

$('#btnDisapproveYes').click(function () {
    var id = $('#DisapproveModal').data('id');
	var dataString = 'id='+ id ;
    $('[data-id=' + id + ']').remove();
    $('#DisapproveModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "disapprove_post.php",
data: dataString,
cache: false,
success: function(html)
{
//$(".fav-count").html(html);
$("#output").html(html);
}
});
//ajax ends
});
//Desapprove
//Feat
$('button.btnMkFeat').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#FeatModal').data('id', id).modal('show');
});

$('#btnFeatYes').click(function () {
    var id = $('#FeatModal').data('id');
	var dataString = 'id='+ id ;
    //$('[data-id=' + id + ']').remove();
    $('#FeatModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "mk_feat_post.php",
data: dataString,
cache: false,
success: function(html)
{
$("#output").html(html);
}
});
//ajax ends
});
//Un Feat
$('button.btnUnFeat').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#unFeatModal').data('id', id).modal('show');
});

$('#btnUnFeatYes').click(function () {
    var id = $('#unFeatModal').data('id');
	var dataString = 'id='+ id ;
    //$('[data-id=' + id + ']').remove();
    $('#unFeatModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "un_feat_post.php",
data: dataString,
cache: false,
success: function(html)
{
$("#output").html(html);
}
});
//ajax ends
});
//Aesapprove
$('button.btnApprove').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').data('id');
    $('#ApproveModal').data('id', id).modal('show');
});

$('#btnApproveYes').click(function () {
    var id = $('#ApproveModal').data('id');
	var dataString = 'id='+ id ;
    $('[data-id=' + id + ']').remove();
    $('#ApproveModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "approve_post.php",
data: dataString,
cache: false,
success: function(html)
{
//$(".fav-count").html(html);
$("#output").html(html);
}
});
//ajax ends
});
//Approve
});
</script>

<section class="col-md-8">

<div id="custom-search-input">
  
    <form class="input-group col-md-12" role="search" name="srch-term" id="srch-term" method="get" action="search_pictures.php">
                                <input type="text" class="search-query form-control" id="term" name="term" placeholder="Tìm kiếm" value="<?php echo $term;?>"/>
                                <input type="hidden" class="search-query form-control" id="status" name="status" value="<?php echo $status;?>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="submit">
                                        <span class=" glyphicon glyphicon-search"></span>
                               </button>
                            </span>
       </form>
                        
</div>

<div id="output"></div>
      
<?php
error_reporting(E_ALL ^ E_NOTICE);
// How many adjacent pages should be shown on each side?
	$adjacents = 5;
	
	$query = $mysqli->query("SELECT COUNT(*) as num FROM media WHERE active='$status' AND title like '%$term%' AND (type=1 or type=2) ORDER BY id DESC");
	
	//$query = $mysqli->query("SELECT COUNT(*) as num FROM photos WHERE  photos.active=1 ORDER BY photos.id DESC");
	
	$total_pages = mysqli_fetch_array($query);
	$total_pages = $total_pages['num'];
	
	$targetpage = "search_pictures.php?term=$term&status=$status";
	$limit = 15; 								//how many items to show per page
	$page = $_GET['page'];
	 
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
 	/* Get data. */
	$result = $mysqli->query("SELECT * FROM media WHERE active='$status' AND title like '%$term%' AND (type=1 or type=2) ORDER BY id DESC LIMIT $start, $limit");
	 
	//$result = $mysqli->query($sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<ul class=\"pagination pagination-lg\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$prev.html\">&laquo;</a></li>";
		else
			$pagination.= "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
				else
					$pagination.= "<li><a href=\"$targetpage?page=$counter.html\">$counter</a></li>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter.html\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1.html\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage.html\">$lastpage</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetpage?page=1.html\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2.html\">2</a></li>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter.html\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1.html\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage.html\">$lastpage</a></li>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<li><a href=\"$targetpage?page=1.html\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2.html\">2</a></li>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter.html\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$next.html\">&raquo;</a></li>";
		else
			$pagination.= "<li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";
		$pagination.= "</ul>\n";		
	}
	
	$q= $mysqli->query("SELECT * FROM media WHERE active='$status' AND title like '%$term%' AND (type=1 or type=2) ORDER BY id DESC limit $start,$limit");


	$numr = mysqli_num_rows($q);
	if ($numr==0)
	{
	?>
    
	<div class="no-results">
	<h3>Tìm kiếm "<span class="tt-text"><?php echo $term;?></span>" không có kết quả nào.</h3>
	<ul class="search-again">
	<li>Hãy chắc chắn nhập đúng từ khóa.</li>
	<li>Thử lại với từ khóa khác.</li>
	<li>Thêm từ khóa vào tìm kiếm.</li>
    <?php if($status==1){ echo "<li>Tìm kiếm trong mục Ảnh chưa duyệt.</li>";}elseif($status==0){ echo "<li>Tìm kiếm trong mục Ảnh đã chưa duyệt.</li>";}?>
	</ul>
	</div>

    <?php
	}
	if ($numr>0)
	{
	?>
       <table class="table table-bordered">

        <thead>

            <tr>
				<th>Thumb</th>
                
                <th>Tiêu đề</th>

                <th>Thời gian</th>

                <th>Quản lý</th>

            </tr>

        </thead>

        <tbody>
    <?php
	}
	
	while($Row=mysqli_fetch_assoc($q)){
	
	$LongTitle = stripslashes($Row['title']);
	$SortTitle = short_title($LongTitle);  
	
	$PhotoLink = convertvn($SortTitle);
	
    $Feat = $Row['feat'];

?>        

            <tr class="btnDelete" data-id="<?php echo $Row['id'];?>">
				<td><a href="../post-<?php echo $Row['id'];?>-<?php echo $PhotoLink;?>.html" target="_blank"><img src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $Row['image'];?>" alt="Image" class="img-responsive" style="width: 50px; height: 50px; object-fit: cover;"></a></td>
                
                <td><a href="../post-<?php echo $Row['id'];?>-<?php echo $PhotoLink;?>.html" target="_blank"><?php echo $SortTitle;?></a></td>

                <td><?php echo get_time_ago(strtotime($Row['date']));?></td>

                <td>
                <!--Testing Modal-->
                
               <button class="btn btn-danger btnDelete">Xóa</button>
               <?php if($status==0){?>
               <button class="btn btn-info btnApprove">Duyệt</button>
               <?php }elseif($status==1){?>
               <button class="btn btn-warning btnDisapprove">Bỏ duyệt</button>
               <?php }?>
               <a class="btn btn-success btnEdit" href="edit_image.php?id=<?php echo $Row['id'];?>">Sửa</a>
               <?php if($status==1){ if($Feat==1){?>
               <button class="btn btn-default btnUnFeat">Bỏ khỏi Feat</button>
               <?php }else if($Feat==0){?>
               <button class="btn btn-primary btnMkFeat">Thêm vào Feat</button>
               <?php } }?>      
                <!--Testing Modal-->
                </td>

            </tr>
<?php } ?>
    
         
        </tbody>

    </table>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Xác nhận</h4>

            </div>
            <div class="modal-body">
				<p>Bạn có chắc chắn muốn Xóa bài đăng?</p>
                <p class="text-warning"><small>Bài đăng không thể khôi phục sau khi xóa.</small></p>		
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDelteYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal --> 

<!--Approve Modal-->
<div class="modal fade" id="DisapproveModal" tabindex="-1" role="dialog" aria-labelledby="DisapproveModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Xác nhận</h4>

            </div>
            <div class="modal-body">
                 <p> Bạn có chắc chắn muốn Bỏ duyệt bài đăng?</p>
				 <p class="text-info"><small>Để thay đổi hãy vào Bài đăng > Ảnh chưa duyệt.</small></p>	
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDisapproveYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Feat Modal-->
<div class="modal fade" id="FeatModal" tabindex="-1" role="dialog" aria-labelledby="FeatModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Xác nhận</h4>

            </div>
            <div class="modal-body">
                 <p> Bạn có chắc chắn muốn Thêm vào Feat?</p>
				 <p class="text-info"><small>Để thay đổi hãy ấn Bỏ khỏi Feat.</small></p>	
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnFeatYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--UnFeat Modal-->
<div class="modal fade" id="unFeatModal" tabindex="-1" role="dialog" aria-labelledby="unFeatModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Xác nhận</h4>

            </div>
            <div class="modal-body">
                 <p> Bạn có chắc chắn muốn Bỏ khỏi Feat?</p>
				 <p class="text-info"><small>Để thay đổi hãy ấn Thêm vào Feat.</small></p>	
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnUnFeatYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


 <!--Approve Modal-->
        <div class="modal fade" id="ApproveModal" tabindex="-1" role="dialog" aria-labelledby="ApproveModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Xác nhận</h4>
              </div>
              <div class="modal-body">
                <p> Bạn có chắc chắn muốn Duyệt bài đăng?</p>
                <p class="text-info"><small>Để thay đổi hãy vào Bài đăng > Video đã duyệt.</small></p>
              </div>
              <!--/modal-body-collapse -->
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnApproveYes">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              </div>
              <!--/modal-footer-collapse --> 
            </div>
            <!-- /.modal-content --> 
          </div>
          <!-- /.modal-dialog --> 
        </div>
        <!-- /.modal --> 

<?php echo $pagination;?>

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>
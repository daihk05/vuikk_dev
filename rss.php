<?php include("header.php");?>

<div class="container">

<div class="col-md-8" id="left">

<div class="post-box" style="margin-left: 10px;">

<header class="post-header"><div class="post-title"><h1>RSS Feeds</h1></div><!--post-title--></header>

<ul class="feeds-nav">

<li><a href="rss_all.html" target="_blank"><span class="fa fa-rss-square"></span> &nbsp;&nbsp; Mới</a></li>

<?php
if($CatSql = $mysqli->query("SELECT * FROM categories ORDER BY cname ASC")){

   
while ($CatROW = mysqli_fetch_array($CatSql)) {
	$id= $CatROW['id'];
	$cname = $CatROW['cname'];
?>

<li><a href="rss_cat-<?php echo $id;?>.html" target="_blank"><span class="fa fa-rss-square"></span> &nbsp;&nbsp; <?php echo $cname;?></a></li>

<?php }

     $CatSql->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}

?>

</ul>

</div><!--post-box-->

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>


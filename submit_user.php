<?php session_start();
include('../db.php');
include('convertvn.php');

if($_POST)
{	
	
	if(!isset($_POST['uName']) || strlen($_POST['uName'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng nhập tên hiển thị.</div>');
	}
	
	if(strlen($_POST['uName'])<3)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Tên hiển thị phải dài hơn 3 ký tự.</div>');
	}

	if(strlen($_POST['uName'])>30)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Tên hiển thị phải ngắn hơn 30 ký tự.</div>');
	}
	
	if(!isset($_POST['uEmail']) || strlen($_POST['uEmail'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng nhập địa chỉ email!</div>');
	}
	
	$email_address = $_POST['uEmail'];
	
	if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
  	// The email address is valid
	} else {
  		die('<div class="alert alert-danger">Email không hợp lệ.</div>');
	}

	//
	$uEmail = $mysqli->escape_string(strtolower($_POST['uEmail']));
	
	if($UserCheck = $mysqli->query("SELECT * FROM users WHERE email ='$uEmail'")){

   	$VdUser 	= mysqli_fetch_array($UserCheck);
	
	$ACV 		= strtolower($VdUser['email']);
	
	$ACTIVE 	= $VdUser['u_active'];

   	$UserCheck->close();
   
	}else{
   
    ?>
	<script>
		errorpage();
	</script>
	<?php

	}
	
	//Check account registered?
	if ($uEmail == $ACV && $ACTIVE == 1)
	{
		die('<div class="alert alert-danger">Email đã tồn tại. Vui lòng nhập email khác!</div>');
	}
	//

	if(!isset($_POST['uPassword']) || strlen($_POST['uPassword'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng nhập mật khẩu!</div>');
	}
	
	if(!isset($_POST['uPassword']) || strlen($_POST['uPassword'])<6)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Mật khẩu phải dài hơn 6 ký tự.</div>');
	}
		if(!isset($_POST['cPassword']) || strlen($_POST['cPassword'])< 1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng nhập lại mật khẩu!</div>');
	}
	
	if ($_POST['uPassword']!== $_POST['cPassword'])
 	{
		//required variables are empty
     	die('<div class="alert alert-danger">Mật khẩu chưa khớp. Vui lòng nhập lại!</div>');
 	
	}
	
	error_reporting(E_ALL ^ E_NOTICE);

	
	//User info
	$UserName  			= mb_ucfirst($mysqli->escape_string($_POST['uName'])); // Username
	$Password  			= $mysqli->escape_string($_POST['uPassword']); // Password
	$EnPassword         = md5($Password); // Encript Password
	$RegDate		    = (new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')))->format('c'); //date
	
	$_SESSION['aEmail'] = $uEmail;

	//Setup active code
	$Code        = rand(1000,9999);

	//Registered but not yet active will update info!
	if ($uEmail == $ACV && $ACTIVE > 1){
		
		$mysqli->query("UPDATE users SET username='$UserName', password='$EnPassword' reg_date='$RegDate', u_active='$Code' WHERE email='$uEmail'");
		
	} else {  //Not yet registered will create new!
	
		$mysqli->query("INSERT INTO users(username, email, password, reg_date, u_active) VALUES ('$UserName', '$uEmail', '$EnPassword', '$RegDate', '$Code')");

	}	


//send email verification
include('send_code.php');

if(!$mail->Send()) {

	die('<div class="alert alert-danger" role="alert">Đã xảy ra lỗi gửi email!</div>');
	
	} else {

?>

	<script type="text/javascript">
	function removeModel() {
	//show activate email
	var aEmail = "<?php echo $_SESSION['aEmail'] ?>";
	document.getElementById("output-email").innerHTML = aEmail;
	//Hide login form
	$('#myModal').modal('hide');
	//Hide register form
	$('#modalRegister').modal('hide');
	
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	//Empty register msg
	$('#output-register').empty();
	//Empty activate msg
	$('#output-activate').empty();
	}
	
	function LoadModel() {
	//Call activate windows
	$('#myActivate').modal('show');
	
	}
	
	setTimeout(removeModel,1500);
	setTimeout(LoadModel,2000);
	
	</script>		
			
		<!--<div class="alert alert-info">Cảm ơn bạn đã đăng ký! Đang chuyển hướng ...</div>-->
		
	<?php	
	}	
}

else {
	
	die('<div class="alert alert-danger">Đã xảy ra sự cố! Vui lòng thử lại!</div>');

} 

?>
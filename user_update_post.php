<?php

include('../db.php');
include('convertvn.php');
?>

<script type="text/javascript" src="js/jquery.form.js"></script>

<?php

$id = $mysqli->escape_string($_GET['id']);

//Get Photo Info
if($Post = $mysqli->query("SELECT * FROM media WHERE id='$id'")){

    $PostRow = mysqli_fetch_array($Post);

	$PostFile = $PostRow['image'];

	$MediaId = $PostRow['id'];

	$PostCategoryId = $PostRow['catid'];
	
    $Post->close();
	
}else{
    
	 printf("Error: %s\n", $mysqli->error);
}


if($_POST)
{	
	if(!isset($_POST['category']) || strlen($_POST['category'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Vui lòng chọn chủ đề!</div>');
	}

	if(!isset($_POST['inputTitle']) || strlen($_POST['inputTitle'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập tiêu đề!</div>');
	}

	//FCatagory and file title and File name
	$Catagory		= $mysqli->escape_string($_POST['category']);
	$FileTitle		= $mysqli->escape_string(mb_ucfirst(nl2br(htmlspecialchars($_POST['inputTitle'])))); //File Title will be used as new File name

   
	$mysqli->query("UPDATE media SET title='$FileTitle',catid='$Catagory' WHERE id='$id'");


	//Get catagory name
	if($cName = $mysqli->query("SELECT cname FROM categories WHERE id=$Catagory")){
		$cNameRow = mysqli_fetch_array($cName);
			$CategoryName = $cNameRow['cname'];
			$CategoryURL = convertvn($CategoryName);
		$cName->close();
	
	}else{
		?><script>errorpage();</script><?php
	}
	

	?>

<script type="text/javascript">

	function removeModel() {
	$('#modelEditPost').modal('hide');
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	$("#output-post").empty();
	$('#FormEditPost').resetForm();

	var id = "<?php echo $MediaId; ?>";
	var title = "<?php echo $FileTitle; ?>"
	$(".title-" + id).html('<div>' + title +'</div>');

	//Category
	var PostCategoryId = "<?php echo $PostCategoryId; ?>";
	var CategoryURL = "<?php echo $CategoryURL; ?>";
	var CategoryName = "<?php echo $CategoryName; ?>";

	$(".cat-" + id).html('<a href="category-' + PostCategoryId + '-' + CategoryURL+ '-1.html"><span class="uname-color">' + CategoryName + '</span></a>');
	}

	setTimeout(removeModel,1500);

</script>
	
	<?php

	die('<div class="alert alert-success" role="alert">Cập nhật xong</div>');

		
   }else{
	   
   		die('<div class="alert alert-danger" role="alert">Đã xảy ra sự cố. Vui lòng thử lại!</div>');
   }

?>
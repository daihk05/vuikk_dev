<?php
include('../db.php');

if($site_settings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

	$Settings 	= mysqli_fetch_array($site_settings);
	  
	  $MFROM 	= stripslashes($Settings['sendemail']);
	  
	  $MPASS 	= stripslashes($Settings['sendpass']);
  
	$site_settings->close();
	  
}else{
	  
	?><script>errorpage();</script><?php
}

if($_POST)
{	
	if(!isset($_POST['inputYourname']) || strlen($_POST['inputYourname'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập họ tên!</div>');
	}
	if(!isset($_POST['inputEmail']) || strlen($_POST['inputEmail'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập email!</div');
	}
	
	$email_address = $_POST['inputEmail'];
	
	if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
  	// The email address is valid
	} else {
  		die('<div class="alert alert-danger" role="alert">Email không hợp lệ..</div>');
	}
	
	if(!isset($_POST['inputSubject']) || strlen($_POST['inputSubject'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập tiêu đề!</div>');
	}
	if(!isset($_POST['inputMessage']) || strlen($_POST['inputMessage'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vui lòng nhập nội dung!</div>');
	}
	
if($SettingsSql = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SettingsSql);
	
	$SettingsSql->close();
	
}else{
    
	?><script>errorpage();</script><?php
}

$SiteContact	     = $Settings['email'];
$FromName		     = $mysqli->escape_string($_POST['inputYourname']);
$FromEmail		 	 = $mysqli->escape_string($_POST['inputEmail']);
$FrominputSubject	 = $mysqli->escape_string($_POST['inputSubject']);
$FromMessage	     = $mysqli->escape_string($_POST['inputMessage']);

require_once('include/class.phpmailer.php');

$mail             = new PHPMailer(); ;

//Setup smtp
$mail->IsSMTP();             
$mail->CharSet  = "utf-8";
$mail->SMTPDebug  = 0;   // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;    // enable SMTP authentication
$mail->SMTPSecure = "ssl";   // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";    // sever gui mail.
$mail->Port       = 465;         // port gui mail de nguyen
// Login smtp
$mail->Username   = $MFROM;  // khai bao dia chi email
$mail->Password   = $MPASS;  // khai bao mat khau

//From
$mail->SetFrom($FromEmail, $FromName);  //Customer email
//send to
$mail->AddAddress($SiteContact);    //Setting email
//subject
$mail->Subject = $FrominputSubject;
//body
$mail->MsgHTML($FromMessage);
//reply to
$mail->AddReplyTo($FromEmail, $FromName);  //customer email

if(!$mail->Send()) {?>

<div class="alert alert-danger" role="alert">Lỗi gửi email!</div>

<?php } else {?>

<script>
	document.getElementById("ContactForm").reset();
</script>

<div class="alert alert-success" role="alert">Cảm ơn bạn đã gửi liên hệ!</div>

<?php }

}

?>
<?php session_start();
include('header_php.php');

// Get Category Info
$cid = $mysqli->escape_string($_GET['cid']);

if($GetCat = $mysqli->query("SELECT * FROM categories WHERE id='$cid'")){

  while($CRow = mysqli_fetch_array($GetCat)){
		$Cid   = $CRow['id'];
		$CDes  = $CRow['description'];
		$CName = $CRow['cname'];
		$CUrl  = convertvn($CName);
  }

$GetCat->close();
	
} else {
	?><script>errorpage();</script><?php
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $CName." - ".$Settings['name']; ?></title>
<meta name="description" content="<?php echo $CDes; ?>" />
<meta name="keywords" content="<?php echo $Settings['keywords']; ?>" />

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fbapp']; ?>" />
<meta property="og:url"             content="<?php echo $Settings['siteurl']; ?>/category-<?php echo $Cid;?>-<?php echo $CUrl;?>-1.html" />
<meta property="og:title"           content="<?php echo $CName; ?>" />
<meta property="og:description" 	  content="<?php echo $CDes; ?>" />
<meta property="og:image"           content="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" />
<!--End Facebook Meta Tags-->


<!--- Require php file --->
<?php 
include('header_js.php');
?>

</head>

<!--- Require php file --->
<?php 
include('header_div.php');
?>


<div class="bottom-header">
  <div class="container">
    <div class="header-bottom-left">
      <div id="slogan">
        <h1><?php echo $CName;?></h1>
      </div>
      <div id="social-love">
        <div id="fb-button-div">
          <div class="fb-like" data-href="<?php echo $Settings['fbpage'];?>" data-width="50px" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>        
      </div>
      <!--/.social-love --> 
      
    </div>
    <!--/.header-bottom-left -->
    
    <div id="search-box">
      <form class="navbar-form" role="search" id="search" method="get" action="search.php">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Tìm kiếm" id="term" name="term">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--center-header--> 
</div>
<!--bottom-header-->

<!-- end -->
<?php session_start();
require_once('google/google-login-api.php');

include('../db.php');

if($site_settings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

	$Settings 	= mysqli_fetch_array($site_settings);
	  
	  $CLIENT_ID 	= stripslashes($Settings['ggapp']);
	  
	  $CLIENT_SECRET 	= stripslashes($Settings['gg_key']);

	  $CLIENT_REDIRECT_URL = $Settings['siteurl'].'/config_gg.php';
  
	$site_settings->close();
	  
}else{
	  
	?><script>errorpage();</script><?php
}

//initialize google API

// Google passes a parameter 'code' in the Redirect Url
if(isset($_GET['code'])) {
	
try {
	$gapi = new GoogleLoginApi();
	
	// Get the access token 
	$data = $gapi->GetAccessToken($CLIENT_ID, $CLIENT_REDIRECT_URL, $CLIENT_SECRET, $_GET['code']);
	
	// Get user information and save the user nformation in session variable
	$_SESSION = $gapi->GetUserProfileInfo($data['access_token']);

	# save the user nformation in session variable

	$_SESSION['snid'] = $snid = $_SESSION['id'];               // To Get Google ID

	$_SESSION['sn_name'] = $_SESSION['name'];     // To Get Google full name

	$_SESSION['sn_email'] = $_SESSION['email'];     // To Get Google email

	$_SESSION['sn_pic'] = $_SESSION['picture'];       // To Get Google avatar

	$_SESSION['provider'] = "GG";  // Social Network Name

}
catch(Exception $e) {
	echo $e->getMessage();
	exit();
}

	
if($UserCheck = $mysqli->query("SELECT * FROM users WHERE snid='$snid'")){
	
	$get_user = mysqli_fetch_array($UserCheck);
	  
	$get_useremail = $get_user['email'];
	  
	$VdUser = $UserCheck->num_rows;
	  
	$UserCheck->close();
	   
}else{
	   
	?><script>errorpage();</script><?php
	  
}
		
/* ---- header location after session ----*/
	  
if ($VdUser == 0 || empty($get_useremail)){
	  
header("Location: register_sn.html");	
	  
} 
	  
else{
	  
$_SESSION['useremail'] = $get_useremail;  //use email session to login

//Save to cookie
$cookie_expiration_time = time() + (86400 * 365);  // for 1 year
setcookie ("useremail", $get_useremail, $cookie_expiration_time);
setcookie ("snid", $snid, $cookie_expiration_time);

//unset old session
unset($_SESSION['snid']);
unset($_SESSION['sn_name']);
unset($_SESSION['sn_pic']);
unset($_SESSION['provider']);
	  
header("Location: .");
	
	}
}

else{

	$loginUrl = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode($CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . $CLIENT_ID . '&access_type=online';
	header("Location: ".$loginUrl);

}

?>
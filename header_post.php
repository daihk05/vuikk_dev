<?php session_start();
include('header_php.php');

//Get Post Data
$DataLink = $Settings['datalink'];

$pid = $mysqli->escape_string($_GET['id']);

if($PostSql = $mysqli->query("SELECT * FROM media WHERE active=1 and id='$pid' LIMIT 1")){

  $PostRow = mysqli_fetch_array($PostSql);

	$MediaId = $PostRow['id'];
	$MediaType = $PostRow['type'];
	$MediaVotes = $PostRow['votes'];
  $PostedBy = $PostRow['uid'];
  $PostCategoryId = $PostRow['catid'];
  $TotalComments = $PostRow['cmts'];

	$LongTitle = stripslashes($PostRow['title']); 
  $MediaTitle = short_title($LongTitle);
	$LongTitle = read_more_title($LongTitle, $MediaId);
	
	$MediaLink = convertvn($MediaTitle);
	$MediaLink = nl2br($MediaLink);

  $MediaUrl = "post-".$MediaId."-".$MediaLink.".html";
  
  $PostUrl = $Settings['siteurl']."/".$MediaUrl;

$PostSql->close();

}else{
  ?><script>errorpage();</script><?php
}

$UpdatePostViews = $mysqli->query("UPDATE media SET views=views+1 WHERE id='$MediaId'");

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo stripslashes($PostRow['title'])." - ".$Settings['name']; ?></title>
<meta name="description" content="<?php echo $Settings['descrp']; ?>" />
<meta name="keywords" content="<?php echo $Settings['keywords']; ?>" />

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fbapp']; ?>" />
<meta property="fb:admins"          content="<?php echo $Settings['fb_key']; ?>"/>
<meta property="og:url"             content="<?php echo $Settings['siteurl']; ?>/post-<?php echo $MediaId;?>-<?php echo $MediaLink; ?>.html" />
<meta property="og:title"           content="<?php echo $MediaTitle; ?>" />
<meta property="og:description" 	  content="<?php echo $Settings['descrp']; ?>" />
<?php if($MediaType==3){?>
<meta property="og:image"           content="<?php echo $Settings['datalink']; ?>/sysimg/logo.png" />
<?php }else{?>
<meta property="og:image"           content="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $PostRow['image']; ?>" />
<?php }?>
<!--End Facebook Meta Tags-->


<style>
/* Comments */
.comments-box {
	margin: 15px 10px auto 5px;
}
.replies-box {
	margin: 10px 0px auto 0px;
}
.form-comment {
	display: inline-block;
}
#comment, #replycomment {
	min-height: 55px;
}
.replyRow {
	margin: 5px -5px 0px 5px;
}
.send-msg, .send-btn {
	margin-top: 5px;
	float: right;
}
.media-comments-body {
	margin-left: 45px;
}
.media-replies-body {
	margin-left: 30px;
}
.comment-vote-box {
	margin: 5px 5px 0px -5px;
	font-size: 12px;
}
.display-vote-comment {
	font-size: 12px;
	font-weight: normal;
	color: #757575;
}
/* Input comments font size  */
.showreplies {
	margin-bottom: 10px;
	font-weight: bold;
}
#more_comment {
	text-align: center;
	margin-top: 5px;
	margin-bottom: -10px;
}
.more_reply {
	margin-bottom: -10px; 
	margin-left: 30px;
}
#load_more {
	text-align: center;
	margin-top: 15px;
}

</style>


<!--- Require php file --->
<?php 
include('header_js.php');

?>
<!--Comments functions-->
<script>
  var datalink = "<?php echo $DataLink; ?>";
</script>
<script type="text/javascript" src="js/jquery.comments.js"></script>


</head>

<!--- Require php file --->
<?php 
include('header_div.php');
?>

<!-- Title line -->
<div class="bottom-header">
  <div class="container">
    <div class="header-bottom-left">
      <div id="slogan"><?php echo $Settings['descrp']; ?></div>
      <div id="social-love">
        <div id="fb-button-div">
          <div class="fb-like" data-href="<?php echo $Settings['fbpage'];?>" data-width="50px" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
      </div>
      <!--/.social-love -->     
    </div>
    <!--/.header-bottom-left -->

    <div id="search-box">
      <form class="navbar-form" role="search" id="search" method="get" action="search.php">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Tìm kiếm" id="term" name="term">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--center-header--> 
</div>
<!--bottom-header-->


<!-- Delete button -->
<div class="modal fade" id="commentDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-deletecmt" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel-deletecmt" style="font-weight:bold; font-size:16px;">Xóa bình luận</h4>

            </div>
            <div class="modal-body">
                <h3>Bạn có chắc chắn muốn Xóa bình luận?</h3>
                <p><small>Nhấn "Xóa" để xác nhận. Nhấn "Hủy" để trở lại.</small></p>
                <p class="text-danger"><small>Nội dung không thể khôi phục sau khi xóa.</small></p>
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
              <div class="col text-center">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy" />
                <input type="button" class="btn btn-danger" id="submitdeletecmt" onclick="delete_comment()" value="Xóa" style="width: 70px;" />
              </div>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- end -->
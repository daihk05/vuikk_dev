<?php include("header.php"); ?>

<div class="container">

<div class="col-md-8" id="left">

<div class="post-box-2">
<header class="post-header"><div class="post-title-2"><h1>Cài đặt</h1></div><!--post-title--></header>

<?php
if(!isset($_SESSION['useremail'])){?>
<script type="text/javascript">
function leave() {
window.location = ".";
}
setTimeout("leave()", 0);
</script>
<?php }else{?>

<script type="text/javascript">

$(document).ready(function()
{

//Upload Profile Photo
$('#inputfile').on('change', function(){
{
    var reader = new FileReader();
    reader.onload = function(e)
    {
        $('#output_image').attr('src', e.target.result);
    }
    reader.readAsDataURL(event.target.files[0]);
    $("#preview").hide();
    $('#output_image').show();
}
});


$('#PictureForm').on('submit', function(e){
    e.preventDefault();
    $('#submitAvatar').attr('disabled', true);
    $("#preview").html('');
    $('#output_image').hide();
    $("#output-msg").html('<div class="alert alert-info">Đang tải lên ...</div>');
    $(this).ajaxSubmit(
    {
        dataType:'json',
        success: function(json) {
        $('#submitAvatar').attr('disabled', false);
        $('#preview').html(json.img);
        $("#preview").show();
        $('#output-msg').html(json.msg);
        if ($('#preview').html() != '') {
            document.getElementById('avatar-img').src = $('#preview').find('img').attr('src'); 
        }
    }
    });
});
});

function addbirthday(caller) {
    var date = $(caller).attr('value'); 
    if (date == '') {
        $(caller).attr('value', '<?php if(empty($ProfileRow['birthday'])){ echo date("Y",strtotime('-16 years'))."-01-01"; } else { echo $ProfileRow['birthday']; } ?>');
    }
}

</script>

<!-- Change avatar -->
<div class="form-box">

<div class="form-box-title"><h3><b>Thay đổi ảnh đại diện</b></h3></div>

<div id="output-msg"></div>

    <form action="avatar.php" id="PictureForm" enctype="multipart/form-data" method="post">
        <!-- begin image label and input -->
        <label>Ảnh (gif, jpg, png)</label>
        <input type="file" name="inputfile" id="inputfile" accept="image/*">
        <!--<input type="file" name="inputfile" id="inputfile" /><-- end image label and input --> 
    
        <div style="text-align: center; margin-top: 15px;">
            <img id="output_image" style="display: none; width: 100px; height: 100px; border-radius: 50%; object-fit: cover;" />
            <div id="preview"></div>
        </div>
        <br>
        <div class="col text-center">
            <button type="submit" class="btn btn-primary" id="submitAvatar">Xác nhận</button>
        </div>
    
    </form><!-- end form -->

</div><!--/. form-box -->

<!-- Change password -->
<div class="form-box">

<div class="form-box-title"><h3><b>Thay đổi thông tin</b></h3></div>

<script>
$(document).ready(function()
{
    $('#FromProfile').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-profile").html('<div class="alert alert-info">Đang xác nhận ...</div>');
        $(this).ajaxSubmit({
        target: '#output-profile',
        success:  afterSuccess //call function after success
        });
    });
});

</script>

<?php

if($Profile = $mysqli->query("SELECT * FROM users WHERE email='$uEmail'")){

    $ProfileRow = mysqli_fetch_array($Profile);

    $UserName = $ProfileRow['username'];
    
	$Gender = $ProfileRow['gender'];
	
	$Provider = $ProfileRow['provider'];
	
	$Profile->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}	

?>

<div id="output-profile"></div>

<form action="submit_profile.php" id="FromProfile" method="post" >

    <div class="form-group">
        <label for="uName">Tên hiển thị</label>
        <input type="text" class="form-control" name="uName" id="uName" value="<?php echo $UserName;?>" placeholder="Nhập tên muốn hiển thị" />
    </div><!--/ form-group -->

    <div class="form-group">    
        <label for="uEmail">Email</label>  
        <input type="text" class="form-control" disabled="disabled" name="uEmail" id="uEmail" value="<?php echo $uEmail;?>" />
    </div><!--/ form-group -->

    <div class="form-group">
	    <label for="sex">Giới tính</label>
        <select class="form-control" name="sex" id="sex">
   	    <?php if(!empty($Gender)){?>
        <option value="<?php echo $Gender;?>"><?php echo $Gender;?></option>
        <?php }?>
        <option value=""> - </option>
	    <option value="Nam">Nam</option>
        <option value="Nữ">Nữ</option>
        <option value="T3">T3</option>
        </select>    
    </div><!--/ form-group -->

    <div class="form-group">    
        <label for="birthday">Ngày sinh</label>
        <input type="date" class="form-control" name="birthday" id="birthday" onclick="addbirthday(this)" value="<?php echo $ProfileRow['birthday'];?>" />
    </div><!--/ form-group -->

    <div class="form-group">    
        <label for="country">Địa chỉ</label>
        <input type="text" class="form-control" name="country" id="country" value="<?php echo $ProfileRow['country'];?>" placeholder="Nhập địa chỉ" />
    </div><!--/ form-group -->

    <div class="col text-center">
    <button type="submit" class="btn btn-primary submitButton">Xác nhận</button>
    </div>

</form>

</div><!--/.form-box -->



<?php if($Provider==NULL){?>

<!-- Change password -->
<div class="form-box">

    <div class="form-box-title"><h3><b>Thay đổi mật khẩu</b></h3></div>

<script>
$(document).ready(function()
{
    $('#FromPassword').on('submit', function(e)
    {
        e.preventDefault();
        $('.submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#outputmsg").html('<div class="alert alert-info">Đang xác nhận ...</div>');
        $(this).ajaxSubmit({
        target: '#outputmsg',
        success:  afterSuccess //call function after success
        });
    });
});

</script>

    <div id="outputmsg"></div>


    <form action="submit_password.php" id="FromPassword" method="post" >

    <div class="form-group" id="form1">
        <label for="nPassword">Mật khẩu hiện tại</label>
        <input type="password" class="form-control" name="nPassword" id="nPassword" placeholder="Nhập mật khẩu hiện tại" />
    </div><!--/ form-group -->

    <div class="form-group">    
        <label for="uPassword">Mật khẩu mới</label>
        <input type="password" class="form-control" name="uPassword" id="uPassword" placeholder="6 ký tự trở lên" />
    </div><!--/ form-group -->

    <div class="form-group">    
        <label for="cPassword">Xác nhận</label>
        <input type="password" class="form-control" name="cPassword" id="cPassword" placeholder="Nhập lại mật khẩu" />
    </div><!--/ form-group -->
    
    <div class="col text-center">
    <button type="submit" class="btn btn-primary submitButton">Xác nhận</button>
    </div>
  
</form>

</div><!--/. form-box -->

<?php } }?>

</div><!--post-box-->

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>


<?php include("header_post.php");?>

<div class="container">

<div class="col-md-8" id="left">

<?php

//Next Link
if($Next = $mysqli->query("SELECT * FROM media WHERE active=1 and type='$MediaType' ORDER BY RAND() LIMIT 1")){
	$NextRow = mysqli_fetch_array($Next);
	$NextCnt = $Next->num_rows;
	$Nid = $NextRow['id'];
	$NextTitle = $NextRow['title'];
	$MediaTitle = short_title($NextTitle);
	$NextLink = convertvn($MediaTitle);
   
    $Next->close();

}else{
	?><script>errorpage();</script><?php
}

//Get category name
if($cName = $mysqli->query("SELECT cname FROM categories WHERE id=$PostCategoryId")){
	$cNameRow = mysqli_fetch_array($cName);
		$CategoryName = $cNameRow['cname'];
		$CategoryURL = convertvn($CategoryName);
	$cName->close();

}else{
	
    ?>
	<script>
		errorpage();
	</script>
	<?php
}


if($Submiter = $mysqli->query("SELECT * FROM users WHERE uid='$PostedBy' limit 1")){

	$SubmiterRow = mysqli_fetch_array($Submiter);
	
	$SubmiterUser = $SubmiterRow['username'];

	$SubmiterUserId = $SubmiterRow['uid'];
	
	$SubmiterLink = convertvn($SubmiterUser);
	
	$UavatarPost = $SubmiterRow['avatar'];

	$Submiter->close();
	
}else{
    
	?>
	<script>
		errorpage();
	</script>
	<?php
}

?>

<div class="post-content-box media-<?php echo $MediaId;?> post-<?php echo $MediaId;?>">
<header class="post-header">
<div class="post-uname">
	<h4>
		<?php if (empty($UavatarPost)){ ?>    
            	<img src="<?php echo $Settings['datalink']; ?>/sysimg/default-avatar.png" alt="" class="avatar-post" style="object-fit: cover;" />
            <?php }elseif (substr($UavatarPost,0,4) == "http"){?>
            	<img src="<?php echo $UavatarPost; ?>" alt="" class="avatar-post" style="object-fit: cover;" />
            <?php }else{?>
            	<img src="<?php echo $Settings['datalink']; ?>/avatars/<?php echo $UavatarPost;?>" alt="" class="avatar-post" style="object-fit: cover;" />
    	<?php }?>

		<a href="profile-<?php echo $PostedBy;?>-<?php echo $SubmiterLink;?>.html">&nbsp;<span class="uname-color"><?php echo $SubmiterUser;?></span></a>&nbsp; <i class="glyphicon glyphicon-triangle-right" style="font-size: 10px; color: #757575;"></i>&nbsp;<a class="cat-<?php echo $MediaId;?>" href="category-<?php echo $PostCategoryId;?>-<?php echo $CategoryURL;?>-1.html"><span class="uname-color"><?php echo $CategoryName;?></span></a>
	</h4>
</div>
<div class="post-title"><h3 class="title-<?php echo $MediaId;?>" style="word-wrap:break-word;"><?php echo $LongTitle;?></h3></div><!--post-title-->
<div class="post-footer"><?php echo show_number($PostRow['views']); ?> lượt xem &bull; <?php echo show_number($TotalComments); ?> bình luận <span style="float: right;"><?php echo get_time_ago(strtotime($PostRow['date']));?></div>
</header>

<div class="post-page-left">

<?php if($MediaType=='1'){ ?>
<div class="post-content">

	<img class="img-responsive" src="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $PostRow['image'];?>" alt="" style="width: 600px;"/>

</div>

<?php } elseif ($MediaType=='2'){ ?>

<div class="post-content">

	<img class="img-responsive" data-gifffer="<?php echo $Settings['datalink']; ?>/uploads/<?php echo $PostRow['image'];?>" alt="" style="width: 600px;">

</div>

<?php } elseif ($MediaType=='3'){

$Host = $PostRow['video_type']; ?>

<a href="<?php echo $MediaUrl;?>" <?php if ($MediaOpen==1){?> target="_blank" <?php }?>>
	<div class="post-content">
	<!-- youtube -->
	<?php if ($Host == "youtube"){?>

		<div class="youtube-post img-responsive">
			<iframe width="600" height="335" src="https://www.youtube.com/embed/<?php echo $PostRow['video_id']; ?>?playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>

	<?php } else if ($Host =="facebook"){?>

		<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/<?php echo $PostRow['video_id']; ?>/" data-show-text="false" data-width="600" data-controls="true"></div>

	<?php } ?>
	</div>
</a>
<?php }?>


<div class="vote-post">

<div class="vote-box" id="binh-luan-<?php echo $MediaId;?>">

<?php if(isset($_SESSION['useremail'])){?>
	<!-- Vote up -->
	<i <?php if (userLiked($MediaId)): ?> class="btn fas fa-thumbs-up"
	   <?php else: ?> class="btn far fa-thumbs-up" <?php endif; ?>
	   data-id="<?php echo $MediaId;?>" onclick="voteup(this)"></i>
	<!-- Show points -->
	<span class="display-vote" data-points="<?php echo $MediaVotes;?>"><?php echo show_number($MediaVotes); ?></span>
	<!-- Vote down -->
	<i <?php if (userDisliked($MediaId)): ?> class="btn fas fa-thumbs-down"
	   <?php else: ?> class="btn far fa-thumbs-down" <?php endif; ?>
	   data-id="<?php echo $MediaId;?>" onclick="votedown(this)"></i>
<?php }else{ ?>
	<!-- Vote up -->
	<i class="btn far fa-thumbs-up" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"></i>
	<!-- Show points -->
	<span class="display-vote"><?php echo show_number($MediaVotes); ?></span>
	<!-- Vote down -->
	<i class="btn far fa-thumbs-down" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"></i>
<?php }?>

</div>
<!--vote-box-->


<div class="dropup pull-center other-btn-post">

  	<i class="btn fas fa-ellipsis-h" role="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

	<ul class="dropdown-menu dropdown-menu-center" role="menu" aria-labelledby="dropdownMenu1">

<?php if($SubmiterUserId == $Uid){?>
	<!-- 1 Edit button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="editpost(this)"><i class="far fa-edit" style="font-size: 16px;"></i>&nbsp; Sửa bài đăng</a></li>
		<li class="divider"></li>
	<!-- 1 Delete button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="deletepost(this)"><i class="far fa-trash-alt" style="font-size: 20px;"></i>&nbsp; Xóa bài đăng</a></li>

<?php } else { ?>
  
	<?php if(isset($_SESSION['useremail'])){?>
		<!-- 1 Report button -->
		<?php if (userReported($MediaId)): ?>
			<li role="presentation" class="disabled"><a><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Đã báo cáo!</a></li>
		<?php else: ?>
    		<li class="reportbtn-<?php echo $MediaId;?>" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="reportpost(this)"><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Báo cáo vi phạm</a></li>
		<?php endif; ?>

	<?php } else { ?>
		<!-- 1 Go to login -->
		<li class="reportbtn-<?php echo $MediaId;?>" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!"><i class="fas fa-shield-virus" style="font-size: 18px;"></i>&nbsp; Báo cáo vi phạm</a></li>

	<?php } ?>
		<li class="divider"></li>
		<!-- 2 Hide post button -->
		<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-id="<?php echo $MediaId;?>" onclick="hidepost(this)"><i class="fas fa-ban" style="font-size: 18px;"></i>&nbsp; Ẩn bài đăng</a></li>

<?php } ?>

		<li class="divider"></li>
	<!-- 3 Facebook share button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="popup('http://www.facebook.com/share.php?u=https://vuikk.com/<?php echo $MediaUrl; ?>&amp;title=<?php echo stripslashes($PostRow['title']); ?>')"><img src="<?php echo $Settings['datalink']; ?>/sysimg/facebook-btn.svg" height="18">&nbsp; Chia sẻ Facebook</a></li>
		<li class="divider"></li>
	<!-- 4 Zalo share button -->
	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="zalo-share-button" data-href="https://vuikk.com/<?php echo $MediaUrl; ?>" data-oaid="579745863508352884" data-customize=true><img src="<?php echo $Settings['datalink']; ?>/sysimg/zalo-btn.svg" height="18">&nbsp; Chia sẻ Zalo</a></li>
	</ul>

</div>


<div class="next-button">
<?php if($NextCnt<1){?>

	<a class="btn btn-link disabled" style="color: #4885ed;"><b>Xem tiếp ></b></a>	
	
	<?php }else{?>

	<a class="btn btn-link" style="color: #4885ed;" href="post-<?php echo $Nid;?>-<?php echo $NextLink;?>.html"><b>Xem tiếp ></b></a>

	<?php } ?>
</div>


</div><!--vote-post-->


<?php

//Ads
if(!empty($Ad3)){?> 
	<div class="ads-three">
		<?php echo $Ad3; ?>
	</div>
<?php }

if (empty($Uavatar)){ 
	$AvatarImg = $Settings['datalink'].'/sysimg/default-avatar.png';
}elseif (substr($Uavatar,0,4) == "http"){
	$AvatarImg = $Uavatar;
}else {
	$AvatarImg = $Settings['datalink'].'/avatars/'.$Uavatar;
}

?>

<div class="comment-panel">

<div class="comments-box">
<h3><b><?php echo number_format($TotalComments, 0, ',', '.'); ?> bình luận</b></h3>
</div>

<div class="comments-box">

	<form id="addCommentsForm" method="post">
    
        <input type="hidden" name="name" id="name" value="<?php echo $Uname;?>" />
            
        <input type="hidden" name="ruid" id="ruid" value="<?php echo $Uid;?>" />
            
        <input type="hidden" name="avatarlink" id="avatarlink" value="<?php echo $AvatarImg;?>" />
            
        <input type="hidden" name="pid" id="pid" value="<?php echo $pid;?>" />

		<div class="form-comment">

			<a href="profile-<?php echo $Uid;?>-<?php echo $Uname;?>.html">
				<img style="width: 35px; height: 35px; border-radius: 50%; object-fit: cover;" src="<?php echo $AvatarImg; ?>" alt="" class="media-object pull-left"/>
 			</a>

           	<div class="media-body">            
				<textarea class="form-control" name="comment" id="comment" rows='2' placeholder="Nhập bình luận"></textarea>
		   	</div>
		   
			<div class="send-msg" style="display: none;">
				<?php if(isset($_SESSION['useremail'])){?>      
		   			<input type="submit" id="sendbtn" class="btn btn-primary btn-sm pull-right" value="Gửi" style="width: 70px;" />	
				<?php } else { ?>
					<input type="button" id="loginbtn" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#myModal" title="Đăng nhập để gửi bình luận!" value="Đăng nhập" style="width: 80px;" />
				<?php } ?>

					<input type="button" id="cancelbtn" class="btn btn-default btn-sm pull-right" onclick="$('.send-msg').hide(); $('#comment').val(''); $('#comment').height(0);" value="Hủy" style="width: 50px; margin-right: 5px;" />
			</div>

		</div>	
    </form>
</div>


<div class="replyRow" style="display: none;">

	<form id="addReplyForm" method="post">

		<input type="hidden" name="namer" id="namer" value="<?php echo $Uname;?>" />
            
   		<input type="hidden" name="ruidr" id="ruidr" value="<?php echo $Uid;?>" />
            
    	<input type="hidden" name="avatarlinkr" id="avatarlinkr" value="<?php echo $AvatarImg;?>" />
            
    	<input type="hidden" name="pidr" id="pidr" value="<?php echo $pid;?>" />

		<input type="hidden" name="parentid" id="parentid" value=""/>

		<input type="hidden" name="touserid" id="touserid" value=""/>

		<input type="hidden" name="tousername" id="tousername" value=""/>
		 
		<div class="form-comment">

			<a href="profile-<?php echo $Uid;?>-<?php echo $Uname;?>.html">
				<img style="width: 20px; height: 20px; border-radius: 50%; object-fit: cover;" src="<?php echo $AvatarImg; ?>" alt="" class="media-object pull-left" />
 			</a>

			<div class="media-body">
        		<textarea class="form-control" name="replycomment" id="replycomment" rows="2" placeholder="Nhập bình luận"></textarea>
			</div>

			<div class="send-btn">
				<input type="submit" id="replybtn" class="btn btn-primary btn-sm pull-right" value="Gửi" style="width: 70px;" />
				<input type="button" id="cancelrepbtn" class="btn btn-default btn-sm pull-right" onclick="$('.replyRow').hide(); $('#replycomment').val(''); $('#replycomment').height(0);" value="Hủy" style="width: 50px; margin-right: 5px;" />
			</div>
		
		</div>
	</form>	
</div>


<div class="editCommentBox" style="display: none; margin-right: -15px;">

	<form id="updateCommentForm" method="post">

		<div class="media">

			<input type="hidden" name="editcid" id="editcid" value="" />

			<div class="media-body">
        		<textarea class="form-control" name="editcomment" id="editcomment" rows="1" placeholder="Nhập bình luận"></textarea>
			</div>

			<div class="send-btn">
				<input type="submit" id="updatecmtbtn" class="btn btn-primary btn-sm pull-right" value="Cập nhật" style="width: 70px;" />
				<input type="button" id="calldeletecmt" class="btn btn-danger btn-sm pull-right" onclick="$('#commentDelete').modal('show');" value="Xóa" style="width: 50px; margin-right: 5px;" />
				<input type="button" id="canceleditbtn" class="btn btn-default btn-sm pull-right" onclick="close_edit_comment()" value="Đóng" style="width: 50px; margin-right: 5px;" />
			</div>
		
		</div>

	</form>
</div>

<!--New comment here-->
<div id="jr"></div>


<?php

$Cnumber = 0;
$Climit = 3;

if($Comments = $mysqli->query("SELECT * FROM comments LEFT JOIN users ON comments.uid=users.uid WHERE comments.pid='$pid' AND comments.parent=0 ORDER BY comments.id DESC")){

	$RowNumbers = mysqli_num_rows($Comments);

   	while ($Comment = mysqli_fetch_array($Comments)){

	$CommentId = $Comment['id'];
	$RepliesNumbers = $Comment['repnum'];

	$CommentUserId = $Comment['uid'];
	$CommentAvatar = $Comment['avatar'];
	$CommentDate = $Comment['date']; 
	$CommentFull = $Comment['comment'];
	$CommentVotes = $Comment['votes'];
	$CommentEdited = $Comment['edited'];

	$CommentUserName = $Comment['username'];
	$CommentUserLink = convertvn($CommentUserName);
	$CommentUserUrl = "profile-".$CommentUserId."-".$CommentUserLink.".html";
   
 ?>
 
 <div class="comments-box" id="comment-<?php echo $Cnumber; ?>" style="display: none;">
 
 	<a href="<?php echo $CommentUserUrl;?>">
 		<?php if (empty($CommentAvatar)){ ?>
			<img style="width: 35px; height: 35px; border-radius: 50%; object-fit: cover;" src="<?php echo $Settings['datalink']; ?>/sysimg/default-avatar.png" alt="" class="media-object pull-left"/>
		<?php }elseif (substr($CommentAvatar,0,4) == "http"){ ?>
			<img style="width: 35px; height: 35px; border-radius: 50%; object-fit: cover;" src="<?php echo $CommentAvatar; ?>" alt="" class="media-object pull-left"/>
		<?php }else { ?>
			<img style="width: 35px; height: 35px; border-radius: 50%; object-fit: cover;" src="<?php echo $Settings['datalink']; ?>/avatars/<?php echo $CommentAvatar; ?>" alt="" class="media-object pull-left"/>
 		<?php } ?>
 	</a>
        
  	<div class="media-comments-body" id="comment-body-<?php echo $CommentId; ?>">
		
		<h4 class="media-heading" style="font-size: 12px;"><a href="<?php echo $CommentUserUrl; ?>"><?php echo $CommentUserName;?></a><span style="color: #999; font-weight: normal;">&nbsp;<?php echo get_time_ago(strtotime($CommentDate));?><span id="cmtEdited-<?php echo $CommentId; ?>"><?php if ($CommentEdited == 1) { echo " &bull; Đã sửa"; } ?></span></span></h4>
		<h5 style="word-wrap:break-word;"><span id="cmtContent-<?php echo $CommentId; ?>"><?php echo $CommentFull;?></span></h5>
                
    	<div class="comment-vote-box">

			<?php if(isset($_SESSION['useremail'])){?>
			<!-- Vote comment up -->
			<i <?php if (cmtLiked($CommentId)): ?> class="btn fas fa-thumbs-up"
				<?php else: ?> class="btn far fa-thumbs-up" <?php endif; ?>
				data-id="<?php echo $CommentId; ?>" onclick="voteup_comment(this)" style="font-size: 14px;"></i>
			<!-- Show comment points -->
			<span class="display-vote-comment" data-points="<?php echo $CommentVotes;?>"><?php echo show_number($CommentVotes); ?></span>
			<!-- Vote comment down -->
			<i <?php if (cmtDisliked($CommentId)): ?> class="btn fas fa-thumbs-down"
				<?php else: ?> class="btn far fa-thumbs-down" <?php endif; ?>
				data-id="<?php echo $CommentId; ?>" onclick="votedown_comment(this)" style="font-size: 14px;"></i>
			<?php }else{ ?>
				<!-- Vote comment up -->
				<i class="btn far fa-thumbs-up" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!" style="font-size: 14px;"></i>
				<!-- Show comment points -->
				<span class="display-vote-comment"><?php echo show_number($CommentVotes); ?></span>
				<!-- Vote comment down -->
				<i class="btn far fa-thumbs-down" data-toggle="modal" data-target="#myModal" title="Vui lòng đăng nhập!" style="font-size: 14px;"></i>
			<?php }

			if(isset($_SESSION['useremail'])){
				if($CommentUserId == $Uid) {?>
					<a href="javascript:void(0)" id="editcmt_content-<?php echo $CommentId; ?>" data-id="<?php echo $CommentId; ?>" data-content="<?php echo str_replace("<br />", "\n", $CommentFull); ?>" onclick="edit_comment(this)" style="color: #065FD4;">Sửa</a>
				<?php } else { ?>	
					<a href="javascript:void(0)" data-parentID="<?php echo $CommentId; ?>" data-toUserId="<?php echo $CommentUserId; ?>" data-toUserName="<?php echo $CommentUserName; ?>" onclick="reply(this)" style="color: #065FD4;">Trả lời</a>
				<?php } 
			} ?>

		</div><!--comment-vote-box-->

	<!--Start Reply and edit here-->
	<?php

	if ($RepliesNumbers > 0) { ?>

		<div class="showreplies">
			<a href="javascript:void(0)" id="show-rep-<?php echo $CommentId; ?>" data-parentId="<?php echo $CommentId; ?>" data-portId="<?php echo $pid; ?>" onclick="show_replies(this)" style="color: #065FD4;">Xem <?php echo number_format($RepliesNumbers, 0, ',', '.'); ?> trả lời <span class="caret"></span></a>
			<a href="javascript:void(0)" id="hide-rep-<?php echo $CommentId; ?>" data-parentId="<?php echo $CommentId; ?>" onclick="hide_replies(this)" type="button" style="display: none; color: #065FD4;">Ẩn <?php echo number_format($RepliesNumbers, 0, ',', '.'); ?> trả lời <span class="dropup"><span class="caret"></span></span></a>
		</div>

		<div id="replies-<?php echo $CommentId; ?>"></div>

	<?php
	} ?>
	<!--End Reply here-->


	</div><!--media-comments-body-->

<!--media-->

</div><!--comment-box-->

<?php

if( $Cnumber < $Climit ){ ?>
	<script>
		var i = "<?php echo $Cnumber; ?>";
		$('#comment-' + i).css( 'display', 'block' );
	</script>
	
	<?php
}

$Cnumber++;
	} //End while
   
?>

<?php

$Comments->close();
   
} else{
   
    printf("<div class='alert alert-danger alert-pull'>Đã xảy ra sự cố. Vui lòng thử lại!</div>");

}         

if($Climit < $RowNumbers){
?>
<div id="more_comment">
	<button class="btn btn-default btn-sm" name="morecmt-btn" id="morecmt-btn" data-id=<?php echo $Climit; ?> data-rows=<?php echo $RowNumbers; ?> type="button">Xem thêm bình luận</button>
</div>
<br>                       
<?php } ?>

 <!--/.Comments -->                           
						<!--</div><--tab_default_2
					</div>
				</div>-->


		</div><!--comment-panel-->
	</div><!--post-page-left-->
</div><!--post-content-box-->
	
	<?php include ("post_side_bar.php");?>


<?php if(!empty($Ad4)){ ?>
	<div class="ads-five" style="margin-top: 15px;">
		<?php echo $Ad4; ?>
	</div>
<?php }?>




</div><!--/.col-md-8 -->

<div class="col-md-4">
	<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->


<?php include("footer.php");

// Check if user already likes post or not
function cmtLiked($cmt_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT * FROM votecmt WHERE uid=$Uid AND cmt_id=$cmt_id AND type='1'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}

// Check if user already dislikes post or not
function cmtDisliked($cmt_id)
{
  global $mysqli;
  global $Uid;
  $sql = "SELECT * FROM votecmt WHERE uid=$Uid AND cmt_id=$cmt_id AND type='2'";
  $result = mysqli_query($mysqli, $sql);
  if (mysqli_num_rows($result) > 0) {
  	return true;
  }else{
  	return false;
  }
}

?>


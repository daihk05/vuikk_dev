<?php
include('../db.php');

if($squ = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($squ);
	$SiteName = htmlentities(strip_tags($Settings['name']));
	$SiteDisc = htmlentities(strip_tags($Settings['descrp']));
	$SiteUrl = $Settings['siteurl'];
    $squ->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}


header("Content-type: text/xml");


echo'<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="https://www.w3.org/2005/Atom">
<channel>
<title>'.$SiteName.' - Latest</title>
<link>'.$SiteUrl.'</link>
<description>'.$SiteDisc.'</description>
<language>en-us</language>';


if($sql = $mysqli->query("SELECT * FROM media WHERE active=1 ORDER BY id DESC LIMIT 8")){

while($row = mysqli_fetch_array($sql)){
	$title = $row['title'];
	$postName = $title;
	
	$postLink = preg_replace("![^a-z0-9]+!i", "-", $postName);
	$postLink = strtolower($postLink);
	
	$pdate = $row['date'];
	$pdate = date('r', strtotime($pdate));

	
	$id = $row['id'];
	$imageName = $row['image'];
	$type = $row['type'];
	
	

$TitleNew = htmlentities(strip_tags($title));

$link= $SiteUrl.'/post-'.$id.'-'.$postLink.'.html';

if($type==3){

$imgurl = $imageName;

}else{
	
$imgurl = $Settings['datalink'].'/uploads/'.$imageName;
	
}

echo '<item>
    <title>'.$TitleNew.'</title>
    <link>'.$link.'</link>
	<guid>'.$link.'</guid>
    <description><![CDATA[ <a href="'.$link.'" rel="self"><img align="left" vspace="4" hspace="6" src="'.$imgurl.'" title="'.$TitleNew.'" alt="'.$TitleNew.'" width="150" /></a>]]></description>
    <pubDate>'.$pdate.'</pubDate>
  </item>';
}
    $sql->close();
}else{
    ?>
	<script>
		errorpage();
	</script>
	<?php
}

echo "</channel></rss>";

?>
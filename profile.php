<?php include("header_profile.php");?>

<div class="container-fluid container-profile">
    <div class="container">
 
<ul id="profile-nav">
<li class="active"><a href="profile-<?php echo $pid;?>-<?php echo $ProfileUsername;?>.html">Bài đăng</a></li> 
<li><a href="upvote-<?php echo $pid;?>-<?php echo $ProfileUsername;?>.html">Quan tâm</a></li>
<!--<li><a href="comments-<?php echo $pid;?>-<?php echo $ProfileUsername;?>.html">Bình luận</a></li>-->
</ul>
    
</div> <!-- /.container --> 
</div> <!-- /.container-fluid -->     

<div class="container">


<script type="text/javascript">

$(document).ready(function()
{
    $('.alert_no_post').delay(3000).fadeOut(1500);
});

</script>

<div class="col-md-8" id="left">

<?php

//Post button
if($pid == $Uid){
	include('post_button.php');
	$LatestSql = $mysqli->query("SELECT * FROM media WHERE active!=0 and uid='$pid' ORDER BY id DESC LIMIT 0, 8");
} else {
	$LatestSql = $mysqli->query("SELECT * FROM media WHERE active=1 and uid='$pid' ORDER BY id DESC LIMIT 0, 8");
}

if(mysqli_num_rows($LatestSql) == 0) { ?>
	<div class="alert_no_post">
		<h3>Chưa có bài đăng nào gần đây!</h3>
	</div>
<?php }

//Show posts
include ("posts_body.php");

$LatestSql->close();

?>

<nav id="page-nav"><a href="data_profile.php?page=2&id=<?php echo $pid;?>"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#left').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.post-box',     //
		loading: {
          				finishedMsg: 'Đã tải xong',
          				img: '<?php echo $Settings['datalink']; ?>/sysimg/ajax-loader.gif'
        			}
		
		
    },
	function(){
		
		$.getScript('js/scroll_load.js');
	
	},
	function(newElements, data, url){
		//Gif Code
		$.getScript('js/ready_functions.js');
	});
</script>

</div><!--/.col-md-8 -->

<div class="col-md-4">
<?php include ("side_bar.php");?>
</div><!--/.col-md-4 -->

</div><!--/.container-->

<?php include("footer.php");?>


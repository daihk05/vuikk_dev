$(document).ready(function(){
	/* The following code is executed once the DOM is loaded */
	
	/* This flag will prevent multiple comment submits: */
	var working = false;
	
	/* Listening for the submit event of the form: */
	$('#addCommentsForm').submit(function(e){

		$('#comment').height(0);

 		e.preventDefault();
		if(working) return false;
		
		working = true;
		$('#sendbtn').val('Đang gửi');
		$('#sendbtn').attr('disabled', true);
		$('span.error').remove();
		
		/* Sending the form fileds to submit php: */
		$.post('submit_comments.php',$(this).serialize(),function(msg){

			working = false;
			$('#sendbtn').val('Gửi');
			$('.send-msg').hide();
			
			if(msg.status){

				/* 
				/	If the insert was successful, add the comment
				/	after the last one on the page with a slideDown effect
				/*/

				$(msg.html).hide().insertAfter('#jr').slideDown();
				$('#comment').val('');
				
			}
			else {

				/*
				/	If there were errors, loop through the
				/	msg.errors object and display them on the page 
				/*/
				
				$.each(msg.errors,function(k,v){
					$('label[for='+k+']').append('<span class="error">'+v+'</span>');
				});
			}
		},'json');

	});


	//Reply
	$('#addReplyForm').submit(function(e){

		$('#replycomment').height(0);

 		e.preventDefault();
		if(working) return false;
		
		working = true;
		$('#replybtn').val('Đang gửi');
		$('#replybtn').attr('disabled', true);
		$('span.error').remove();
		
		/* Sending the form fileds to submit php: */
		$.post('submit_replies.php',$(this).serialize(),function(msg){

			working = false;
			$('#replybtn').val('Gửi');
			$('.replyRow').hide();
			
			if(msg.status){

				/* 
				/	If the insert was successful, add the comment
				/	below the last one on the page with a slideDown effect
				/*/

				$(msg.html).hide().insertAfter('.replyRow').slideDown();
				$('#replycomment').val('');
				
			}
			else {

				/*
				/	If there were errors, loop through the
				/	msg.errors object and display them on the page 
				/*/
				
				$.each(msg.errors,function(k,v){
					$('label[for='+k+']').append('<span class="error">'+v+'</span>');
				});
			}
		},'json');

	});


	//Edit comment
	$('#updateCommentForm').submit(function(e){

		e.preventDefault();
		 
		var cid = $('#editcid').val();
		
		if(working) return false;
		
		working = true;
		$('span.error').remove();
		
		/* Sending the form fileds to submit php: */
		$.post('submit_comment_update.php',$(this).serialize(),function(msg){

			working = false;
			
			$('.editCommentBox').hide();
			$('#comment-body-' + cid).show();
			$('#editcmt_content-' + cid).attr('data-content', $('#editcomment').val());
			
			if(msg.status){

				/* If the insert was successful, add new content to comment	/*/

				$('#cmtEdited-' + cid).html('<span> &bull; Đã sửa</span>');
				$('#cmtContent-' + cid).html($(msg.html));
				
			}
			else {

				/*
				/	If there were errors, loop through the
				/	msg.errors object and display them on the page 
				/*/
				
				$.each(msg.errors,function(k,v){
					$('label[for='+k+']').append('<span class="error">'+v+'</span>');
				});
			}
		},'json');

	});


//Load more posts 
$(document).on('click', '#btn_more', function(){ 
    $('#load_more').html('<div style="text-align: center;"><img src="' + datalink + '/sysimg/ajax-loader.gif"></div>');
    	$.ajax({  
           	url:"post_side_loadmore.php",  
			
            success:function(data)  
            {  
                if(data != '')  
                {  
					$('#load_more').remove();
					$('#load_data_table').append(data);
                }  
                else  
                {  
					$('#load_more').html('Đã tải xong');
					$('#load_more').delay(2000).fadeOut('slow');
                }  
            }  
       	});  
});


//Load more comments
$(document).on('click', '#morecmt-btn', function(){ 
	var id = $(this).data('id');
	var rows = $(this).data('rows');
	var limit = id + 8;

	for ( i = id; i < limit; i++ ){
		$('#comment-' + i).css( 'display', 'block' );
	}

	if(limit < rows){		
		$('#more_comment').html('<button class="btn btn-default btn-sm" name="morecmt-btn" id="morecmt-btn" data-id="' + limit + '" data-rows="' + rows + '" type="button">Xem thêm bình luận</button>');
	} else {
		$('#more_comment').remove();
	}

});


//Load more replies 
$(document).on('click', '#morerep-btn', function(){
	var p = $(this).data('parent');
	var id = $(this).data('id');
	var rows = $(this).data('rows');
	var limit = id + 8;

	for ( i = id; i < limit; i++ ){
		$('#comment-' + p + '-' + i).css( 'display', 'block' );
	}

	if(limit < rows){		
		$('.more_reply').html('<a style="font-size: 14px; color: #337AB7;" href="javascript:void(0)" name="morerep-btn" id="morerep-btn" data-parent="' + p + '" data-id="' + limit + '" data-rows="' + rows + '" type="button">Xem thêm trả lời</a>');
	} else {
		$('.more_reply').remove();
	}

});


//Show send button if textarea clicked
$('#comment').on('click', function (e) {
	e.preventDefault();
	$('.send-msg').show();
});

//Enable submit button if textarea not empty
$('#sendbtn').attr('disabled', true);
$('#replybtn').attr('disabled', true);
$('input[type="text"],textarea').on('keyup', function () {
    var comment_value = $('#comment').val();
    var reply_value = $('#replycomment').val();
	
	if (comment_value != '') {
        $('#sendbtn').removeAttr('disabled');
	} else {
        $('#sendbtn').attr('disabled', true);
    }
	
	if (reply_value != '') {
		$('#replybtn').removeAttr('disabled');
	} else {
        $('#replybtn').attr('disabled', true);
    }
});

	
	}); //Closed ready(function())


//Vote comment function
function voteup_comment(caller){

  display = $(caller).siblings('span.display-vote-comment');
  votedownbtn = $(caller).siblings('i.fa-thumbs-down');
  var id = $(caller).attr('data-id');
  var p = parseInt(display.attr('data-points'));

  if ($(caller).hasClass('fas')) {
    //action = 'unlike';
    $(caller).removeClass('fas');
    $(caller).addClass('far');
    p = p-1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else if ( ($(caller).hasClass('far')) && votedownbtn.hasClass('fas') ) {
    //action = 'changetolike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    votedownbtn.removeClass('fas');
    votedownbtn.addClass('far');
    p = p+2;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else {
    //action = 'like';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    p = p+1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }    
  }

  var dataString = { id:id, p:p };

  $.ajax({
    type: "POST",
		url: "vote_comments_up.php",
		data: dataString,
		cache: false,
          success: function(html){              
              if ( p > 999 ) {
                display.html(html);
              }
          }
  });		
}

function votedown_comment(caller){

  display = $(caller).siblings('span.display-vote-comment');
  voteupbtn = $(caller).siblings('i.fa-thumbs-up');
  var id = $(caller).attr('data-id');
  var p = parseInt(display.attr('data-points'));  

  if ($(caller).hasClass('fas')) {
    //action = 'undislike';
    $(caller).removeClass('fas');
    $(caller).addClass('far');
    p = p+1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else if ( ($(caller).hasClass('far')) && voteupbtn.hasClass('fas') ) {
    //action = 'changetodislike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    voteupbtn.removeClass('fas');
    voteupbtn.addClass('far');
    p = p-2;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  } else {
    //action = 'dislike';
    $(caller).removeClass('far');
    $(caller).addClass('fas');
    p = p-1;
    display.attr('data-points', p);
    if (p < 1000) { display.html(p); }
  }

  var dataString = { id:id, p:p };

  $.ajax({
    type: "POST",
		url: "vote_comments_down.php",
		data: dataString,
		cache: false,
          success: function(html){              
              if ( p > 999 ) {
                display.html(html);
              }
          }
  });		
}

//Show replies

function show_replies(caller) {
	var pid = $(caller).attr('data-portId');
	var cid = $(caller).attr('data-parentId');
	var dataString = { pid:pid, cid:cid };

	$('#show-rep-' + cid).hide();
	$('#hide-rep-' + cid).show();
	$('#replies-' + cid).show();

	if ( $('#replies-' + cid).text().length == 0 ) {
	// length is 0

	$('#replies-' + cid).html('<div style="text-align: center;"><img src="' + datalink + '/sysimg/ajax-loader.gif"></div>');

	$.ajax({
		type: "POST",
		url: "load_replies.php",
		data: dataString,
		cache: false,

		success: function(html)
		{
			$('#replies-' + cid).html(html);
		}
		});
	}
}	

//Hide replies
function hide_replies(caller) {
	var cid = $(caller).attr('data-parentId');

	$('#show-rep-' + cid).show();
	$('#hide-rep-' + cid).hide();
	$('#replies-' + cid).hide();

}


//Call reply box
function reply(caller) {
	var parentID = $(caller).attr('data-parentID');
	var touserID = $(caller).attr('data-toUserId');
	var touserName = $(caller).attr('data-toUserName');

	$('.replyRow').insertAfter($(caller));
	$('#parentid').val(parentID);
	$('#touserid').val(touserID);
	$('#tousername').val(touserName);
	$('#replycomment').val('');
	$('#replycomment').height(0);
	$('.replyRow').show();
}


//Call edit comment box
function edit_comment(caller) {
	var cid = $(caller).attr('data-id');
	var cmt = $(caller).attr('data-content');

	//Add cid and comment content to edit Form
	$('#editcid').val(cid);
	$('#editcomment').val(cmt);

	//Add commentId to delete button
	$('#commentDelete').attr('data-id', cid);
	
	//Add edit form
	$('.editCommentBox').insertAfter('#comment-body-' + cid);
	
	//Hide current editting comment body
	$('#comment-body-' + cid).hide();

	//Add content to textarea
	$('#editcomment').val('');
	$('#editcomment').height(0);
	$('.editCommentBox').show();
	$('#editcomment').focus();
	$('#editcomment').val(cmt);

	//Input text auto resize height
	$('#editcomment').each(function () {
		this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
  	}).on('input', function () {
		this.style.height = 'auto';
		this.style.height = (this.scrollHeight) + 'px';
  	});
  
}

//Close edit comment
function close_edit_comment() {
	$('.editCommentBox').hide();
	$('#editcomment').val('');
	$('.media-comments-body').show();
	$('.media-replies-body').show();
}

//Submit delete comment
function delete_comment() {
	var cid = $('#commentDelete').attr('data-id');
	var dataString = 'cid=' + cid ;
	$('#commentDelete').modal('hide');

	$.ajax({
		type: "POST",
		url: "submit_comment_delete.php",
		data: dataString,
		cache: false,

		success: function(html)
		{
			$('.editCommentBox').hide();
			$('#comment-body-' + cid).html('<div style="margin-bottom: 45px;">' + html + '</div>');
			$('.media-comments-body').show();
			$('.media-replies-body').show();
		}
	});
}

//Update edit comment
function update_comment(caller) {
	var cid = $(caller).attr('data-id');
	
	$('#editcomment').val(cid);
}

